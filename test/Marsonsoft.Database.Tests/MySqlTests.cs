﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Marsonsoft.Database.Tests
{
    public class MySqlTests
    {
        private Configuration.DatabaseConfiguration CreateConfig()
        {
            return new Configuration.DatabaseConfiguration
            {
                DefaultName = "defaultConnection",
                DefaultProviderName = TestData.Providers.MySql.InvariantName,
                Connections = new List<Configuration.Connection>
                {
                    new Configuration.Connection
                    {
                        Name = "defaultConnection",
                        ProviderName = TestData.Providers.MySql.InvariantName,
                        ConnectionString = TestData.ConnectionStrings.MySqlFirst
                    }
                },
                Providers = new List<Configuration.Provider>
                {
                    TestData.Providers.MySql
                }
            };
        }

        [Fact]
        public void GetDbProvider_IsCorrectNamespace()
        {
            var config = CreateConfig();
            var provider = DataEnvironmentProviderFactory.Create(config);
            var dataEnvironment = provider.GetDefault();

            Assert.StartsWith("MySql.", dataEnvironment.DbProviderFactory.GetType().FullName);
        }

        [Fact]
        public void GetConnection_IsCorrectNamespace()
        {
            var config = CreateConfig();
            var provider = DataEnvironmentProviderFactory.Create(config);
            var dataEnvironment = provider.GetDefault();
            var connection = dataEnvironment.ConnectionProvider.CreateConnection();
            Assert.StartsWith("MySql.", connection.GetType().FullName);
        }

    }
}
