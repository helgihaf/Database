﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Marsonsoft.Database.Tests
{
    public class DataEnvironmentProviderFactoryTests
    {
        [Fact]
        public void CreateDataEnvironmentProvider_NullConfig_ThrowsException()
        {
            Assert.Throws<ArgumentNullException>("databaseConfiguration", () =>
                DataEnvironmentProviderFactory.Create(null));
        }

        [Fact]
        public void CreateDataEnvironmentProvider_EmptyConfig_ThrowsException()
        {
            Assert.Throws<ArgumentException>("databaseConfiguration", () =>
                DataEnvironmentProviderFactory.Create(new Configuration.DatabaseConfiguration()));
        }

        [Fact]
        public void CreateDataEnvironmentProvider_OnlyProvidersList_Ok()
        {
            var config = new Configuration.DatabaseConfiguration();
            config.Providers = new List<Configuration.Provider> { TestData.Providers.MySql };

            var provider = DataEnvironmentProviderFactory.Create(config);
            Assert.NotNull(provider);
        }

        [Fact]
        public void CreateDataEnvironmentProvider_DbFactoriesIsNull_ThrowsException()
        {
            var config = new Configuration.DatabaseConfiguration();
            Assert.Throws<ArgumentNullException>("dbProviderFactories", () =>
                DataEnvironmentProviderFactory.Create(config, null));
        }

        [Fact]
        public void CreateDataEnvironmentProvider_DbFactoriesUsed_Ok()
        {
            var mockFactory = new Mock<IDbProviderFactoryFactory>();
            var config = new Configuration.DatabaseConfiguration();

            var provider = DataEnvironmentProviderFactory.Create(config, mockFactory.Object);
            Assert.NotNull(provider);
        }
    }
}
