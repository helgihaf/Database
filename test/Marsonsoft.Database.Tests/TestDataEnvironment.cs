﻿using System;
using System.Data.Common;

namespace Marsonsoft.Database.Tests
{
    [ProviderSupport("Some.Imaginary.Provider")]
    public class TestDataEnvironment : IDataEnvironment
    {
        public TestDataEnvironment(DbProviderFactory dbProviderFactory, IConnectionProvider connectionProvider)
        {
            DbProviderFactory = dbProviderFactory ?? throw new ArgumentNullException(nameof(dbProviderFactory));
            ConnectionProvider = connectionProvider ?? throw new ArgumentNullException(nameof(connectionProvider));
            Sql = new SqlFactoryAnsi92();
        }

        public IConnectionProvider ConnectionProvider { get; private set; }

        public DbProviderFactory DbProviderFactory { get; private set; }

        public ITextValidator TextValidator { get; private set; }

        public ISqlFactory Sql { get; private set; }
    }
}
