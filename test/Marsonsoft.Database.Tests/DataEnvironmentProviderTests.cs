﻿using Moq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xunit;

namespace Marsonsoft.Database.Tests
{
    public class DataEnvironmentProviderTests
    {
        [Fact]
        public void DataEnvironmentProvider_ConfigRemembered_Ok()
        {
            var config = CreateConfigWithSingleDbProvider();

            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Equal(config, provider.Configuration);
        }

        [Fact]
        public void GetDefault_NoConnectionsSpecified_ThrowsException()
        {
            var config = CreateConfigWithSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Throws<InvalidOperationException>(() => provider.GetDefault());
        }

        [Fact]
        public void GetDefault_NoDefaultConnectionNameSpecified_ThrowsException()
        {
            var config = CreateConfigWithSingleConnectionAndSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Throws<InvalidOperationException>(() => provider.GetDefault());
        }

        [Fact]
        public void GetDefault_ConfigCorrect_Ok()
        {
            var config = CreateConfigWithSingleConnectionAndSingleDbProvider();
            config.DefaultName = config.Connections.First().Name;
            var provider = DataEnvironmentProviderFactory.Create(config);

            var dataEnvironment = provider.GetDefault();

            Assert.NotNull(dataEnvironment);
        }

        [Fact]
        public void GetByConnectionStringName_ConfigCorrect_Ok()
        {
            var config = CreateConfigWithSingleConnectionAndSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            var dataEnvironment = provider.GetByConnectionStringName(config.Connections.First().Name);

            Assert.NotNull(dataEnvironment);
        }

        [Fact]
        public void GetByConnectionStringName_NoDefaultProvider_ThrowsException()
        {
            var config = CreateConfigWithSingleDbProvider();
            config.Connections = new List<Configuration.Connection>
            {
                new Configuration.Connection { Name = "connection1", ConnectionString = TestData.ConnectionStrings.MySqlFirst }
            };

            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Throws<InvalidOperationException>(() =>
                provider.GetByConnectionStringName(config.Connections.First().Name));
        }

        [Fact]
        public void GetByConnectionStringName_NameIsNull_ThrowsException()
        {
            var config = CreateConfigWithSingleConnectionAndSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Throws<ArgumentNullException>("name", () => provider.GetByConnectionStringName(null));
        }

        [Fact]
        public void GetByConnectionStringName_NameNotExists_ThrowsException()
        {
            var config = CreateConfigWithSingleConnectionAndSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Throws<ArgumentException>("name", () => provider.GetByConnectionStringName(Guid.NewGuid().ToString()));
        }

        [Fact]
        public void GetByConnectionStringName_MultipleConnectionStringSameProvider_Ok()
        {
            var config = new Configuration.DatabaseConfiguration();
            config.Connections = new List<Configuration.Connection>
            {
                new Configuration.Connection
                {
                    Name = "connection1",
                    ProviderName = TestData.Providers.MySql.InvariantName,
                    ConnectionString = TestData.ConnectionStrings.MySqlFirst
                },
                new Configuration.Connection
                {
                    Name = "connection2",
                    ProviderName = TestData.Providers.MySql.InvariantName,
                    ConnectionString = TestData.ConnectionStrings.MySqlSecond
                },
                new Configuration.Connection
                {
                    Name = "connection3",
                    ProviderName = TestData.Providers.MySql.InvariantName,
                    ConnectionString = TestData.ConnectionStrings.MySqlThird
                }
            };
            config.Providers = new List<Configuration.Provider> { TestData.Providers.MySql };
            var provider = DataEnvironmentProviderFactory.Create(config);

            for (int i = 0; i < 3; i++)
            {
                string connectionName = "connection" + (i + 1);
                var env = provider.GetByConnectionStringName(connectionName);
                using (var connection = env.ConnectionProvider.CreateConnection())
                {
                    Assert.Equal(config.Connections[i].ConnectionString, connection.ConnectionString, StringComparer.OrdinalIgnoreCase);
                }
            }
        }


        [Fact]
        public void GetByConnectionStringName_MultipleConnectionStringDifferentProviders_Ok()
        {
            var config = new Configuration.DatabaseConfiguration();
            config.Connections = new List<Configuration.Connection>
            {
                new Configuration.Connection
                {
                    Name = "connection1",
                    ProviderName = TestData.Providers.MySql.InvariantName,
                    ConnectionString = TestData.ConnectionStrings.MySqlFirst
                },
                new Configuration.Connection
                {
                    Name = "connection2",
                    ProviderName = TestData.Providers.SqlServer.InvariantName,
                    ConnectionString = TestData.ConnectionStrings.SqlServerFirst
                }
            };
            config.Providers = new List<Configuration.Provider>
            {
                TestData.Providers.MySql,
                TestData.Providers.SqlServer
            };
            var provider = DataEnvironmentProviderFactory.Create(config);

            AssertByConnectionStringName(provider, "connection1", "MySql.");
            AssertByConnectionStringName(provider, "connection2", "System.Data.SqlClient.");
        }

        private static void AssertByConnectionStringName(IDataEnvironmentProvider provider, string connectionName, string namespacePrefix)
        {
            var dataEnvironment = provider.GetByConnectionStringName(connectionName);
            var connection = dataEnvironment.ConnectionProvider.CreateConnection();
            Assert.StartsWith(namespacePrefix, connection.GetType().FullName);
        }

        [Fact]
        public void GetByConnectionSetting_SingleProviderConfig_Ok()
        {
            var config = CreateConfigWithSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            var dataEnvironment = provider.GetByConnectionSetting(config.Providers.First().InvariantName, TestData.ConnectionStrings.MySqlFirst);

            Assert.NotNull(dataEnvironment);
        }

        [Fact]
        public void GetByConnectionSetting_SingleProviderConfigNullProviderName_Ok()
        {
            var config = CreateConfigWithSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Throws<ArgumentNullException>("providerInvariantName", () => provider.GetByConnectionSetting(null, TestData.ConnectionStrings.MySqlFirst));
        }

        [Fact]
        public void GetByConnectionSetting_SingleProviderConfigNullCString_Ok()
        {
            var config = CreateConfigWithSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Throws<ArgumentNullException>("connectionString", () => provider.GetByConnectionSetting(config.Providers.First().InvariantName, null));
        }

        [Fact]
        public void GetByConnectionSetting_SingleProviderConfigProviderNotExists_Ok()
        {
            var config = CreateConfigWithSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            Assert.Throws<ArgumentException>("providerInvariantName", () => provider.GetByConnectionSetting(Guid.NewGuid().ToString(), TestData.ConnectionStrings.MySqlFirst));
        }

        [Fact]
        public void Add_ValidProvider_Ok()
        {
            var config = CreateConfigWithSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);

            provider.Add(typeof(TestDataEnvironment));
        }

        [Fact]
        public void Add_InvalidProvider_ThrowsException()
        {
            var config = CreateConfigWithSingleDbProvider();
            var provider = DataEnvironmentProviderFactory.Create(config);
            var dataEnvironmentMock = new Mock<IDataEnvironment>();

            Assert.Throws<ArgumentException>("type", () => provider.Add(dataEnvironmentMock.Object.GetType()));
        }

        private Configuration.DatabaseConfiguration CreateConfigWithSingleDbProvider()
        {
            var config = new Configuration.DatabaseConfiguration();
            config.Providers = new List<Configuration.Provider> { TestData.Providers.MySql };

            return config;
        }

        private Configuration.DatabaseConfiguration CreateConfigWithSingleConnectionAndSingleDbProvider()
        {
            var config = CreateConfigWithSingleDbProvider();
            config.Connections = new List<Configuration.Connection>
            {
                new Configuration.Connection { Name = "connection1", ProviderName = TestData.Providers.MySql.InvariantName, ConnectionString = TestData.ConnectionStrings.MySqlFirst }
            };

            return config;
        }
    }
}
