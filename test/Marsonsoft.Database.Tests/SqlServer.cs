﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using Xunit;

namespace Marsonsoft.Database.Tests
{
    public class SqlServer
    {
        private Configuration.DatabaseConfiguration CreateConfig()
        {
            return new Configuration.DatabaseConfiguration
            {
                DefaultName = "defaultConnection",
                DefaultProviderName = TestData.Providers.SqlServer.InvariantName,
                Connections = new List<Configuration.Connection>
                {
                    new Configuration.Connection
                    {
                        Name = "defaultConnection",
                        ProviderName = TestData.Providers.SqlServer.InvariantName,
                        ConnectionString = TestData.ConnectionStrings.SqlServerFirst
                    }
                },
                Providers = new List<Configuration.Provider>
                {
                    TestData.Providers.SqlServer
                }
            };
        }

        [Fact]
        public void GetDbProvider_IsCorrectNamespace()
        {
            var config = CreateConfig();
            var provider = DataEnvironmentProviderFactory.Create(config);
            var dataEnvironment = provider.GetDefault();

            Assert.StartsWith("System.Data.SqlClient.", dataEnvironment.DbProviderFactory.GetType().FullName);
        }

        [Fact]
        public void GetConnection_IsCorrectNamespace()
        {
            var config = CreateConfig();
            var provider = DataEnvironmentProviderFactory.Create(config);
            var dataEnvironment = provider.GetDefault();
            var connection = dataEnvironment.ConnectionProvider.CreateConnection();
            Assert.StartsWith("System.Data.SqlClient.", connection.GetType().FullName);
        }
    }
}
