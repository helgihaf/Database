﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Database.Tests.TestData
{
    internal static class Providers
    {
        public static readonly Configuration.Provider MySql = new Configuration.Provider
        {
            InvariantName = "MySql.Data.MySqlClient",
            AssemblyQualifiedName = "MySql.Data.MySqlClient.MySqlClientFactory, MySql.Data, Version=6.10.4.0, Culture=neutral, PublicKeyToken=c5687fc88969c44d"
        };

        public static readonly Configuration.Provider SqlServer = new Configuration.Provider
        {
            InvariantName = "System.Data.SqlClient",
            AssemblyQualifiedName = "System.Data.SqlClient.SqlClientFactory, System.Data.SqlClient, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
        };
    }
}
