﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Database.Tests.TestData
{
    internal static class ConnectionStrings
    {
        public const string MySqlFirst = "server=myServerAddress;database=myDataBase;user id=myUsername;password=myPassword";
        public const string MySqlSecond = "server=mySecondServerAddress;database=mySecondDataBase;user id=mySecondUsername;password=mySecondPassword";
        public const string MySqlThird = "server=myThirdServerAddress;database=myThirdDataBase;user id=myThirdUsername;password=myThirdPassword";
        public const string SqlServerFirst = "Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=myPassword;";
        public const string SqlServerSecond = "Server=mySecondServerAddress;Database=mySecondDataBase;Trusted_Connection=True;";
    }
}
