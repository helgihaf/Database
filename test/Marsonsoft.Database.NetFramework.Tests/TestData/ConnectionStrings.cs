﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Database.NetFramework.Tests.TestData
{
    internal static class ConnectionStrings
    {
        public const string OracleFirst = "Data Source=MyOracleDB;User Id=myUsername;Password=myPassword;";
        public const string OracleSecond = "Data Source=MySecondOracleDB;User Id=mySecondUsername;Password=mySecondPassword;";
    }
}
