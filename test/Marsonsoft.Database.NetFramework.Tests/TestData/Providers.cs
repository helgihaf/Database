﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marsonsoft.Database.NetFramework.Tests.TestData
{
    internal static class Providers
    {
        public static readonly Configuration.Provider OracleManaged = new Configuration.Provider
        {
            InvariantName = "Oracle.ManagedDataAccess.Client",
            AssemblyQualifiedName = "Oracle.ManagedDataAccess.Client.OracleClientFactory, Oracle.ManagedDataAccess, Version=4.122.1.0, Culture=neutral, PublicKeyToken=89b483f429c47342"
        };

        public static readonly Configuration.Provider OracleNative = new Configuration.Provider
        {
            InvariantName = "Oracle.DataAccess.Client",
            AssemblyQualifiedName = "Oracle.DataAccess.Client.OracleClientFactory, Oracle.DataAccess, Version=2.112.1.0, Culture=neutral, PublicKeyToken=89b483f429c47342"
        };
    }
}
