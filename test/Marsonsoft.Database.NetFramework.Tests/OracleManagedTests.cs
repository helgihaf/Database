﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;

namespace Marsonsoft.Database.NetFramework.Tests
{
    [TestClass]
    public class OracleManagedTests
    {
        private Configuration.DatabaseConfiguration CreateConfig()
        {
            return new Configuration.DatabaseConfiguration
            {
                Connections = new List<Configuration.Connection>
                {
                    new Configuration.Connection
                    {
                        Name = "connection1",
                        ProviderName = TestData.Providers.OracleManaged.InvariantName,
                        ConnectionString = TestData.ConnectionStrings.OracleFirst
                    }
                },
                Providers = new List<Configuration.Provider>
                {
                    TestData.Providers.OracleManaged
                }
            };
        }

        [TestMethod]
        public void GetDbProvider_IsCorrectNamespace()
        {
            var config = CreateConfig();
            var provider = DataEnvironmentProviderFactory.Create(config);
            var dataEnvironment1 = provider.GetByConnectionStringName("connection1");

            AssertStartsWith(TestData.Providers.OracleManaged.InvariantName, dataEnvironment1.DbProviderFactory.GetType().FullName);
        }

        [TestMethod]
        public void GetConnection_IsCorrectNamespace()
        {
            var config = CreateConfig();
            var provider = DataEnvironmentProviderFactory.Create(config);
            var dataEnvironment = provider.GetByConnectionStringName("connection1");
            var connection = dataEnvironment.ConnectionProvider.CreateConnection();

            AssertStartsWith(TestData.Providers.OracleManaged.InvariantName, connection.GetType().FullName);
        }

        private void AssertStartsWith(string expectedStart, string fullString)
        {
            Assert.IsTrue(fullString.StartsWith(expectedStart));
        }
    }
}
