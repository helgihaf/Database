﻿# Marsonsoft.Database [![Build status](https://ci.appveyor.com/api/projects/status/jws0dh79my9bp7rk?svg=true)](https://ci.appveyor.com/project/helgihaf/database) [![NuGet](https://badge.fury.io/nu/Marsonsoft.Database.svg)](https://www.nuget.org/packages/Marsonsoft.Database) [![License](https://img.shields.io/github/license/mashape/apistatus.svg)](https://en.wikipedia.org/wiki/MIT_License)
## About
This is a small library that can simplify your database code and make it more database provider agnostic.

## Background
Database provider agnostic code enables you to write code that communicates with a database without direct dependency on specific types of providers.
For example, you can write code that directly creates objects in the `Oracle.DataAccess.Client` namespace and that code will be directly dependent upon the Oracle.DataAccess.Client.dll.
Alternatively you can write your code agains the standard `System.Data` namespace and decide at runtime which database provider you will be using, be it Oracle.DataAccess.Client or the newer Oracle.ManagedDataAccess.Client. 

But this is all possible without using this library. However, this library makes it a little bit easier to follow this path.

At the center of the library is an interface called `IDataEnvironment`. You create this environment by providing a connection string and a database provider factory. Using this interface you can:
* Create or open a database connection,
* Access the correct `System.Data.Common.DbProviderFactory` for your connection type,
* Access a minimal SQL statement factory for creating SQL statements suited for the connection type, and
* Validate SQL identifiers.

## Feature Overview
* Easy method of obtaining connection strings and provider from config file.
* Automatic creation of data provider based on config data.
* Easy to obtain correct connection type for either default connection string or a named connection string.
* Automatic opening of connections with the ability to run custom code when a connection is opened.
* Convenience methods for adding parameters to DbCommand-s.
* Convenience methods for getting values from DbDataReader-s.
* Convenience methods to handle Oracle specific types (ref cursors).
* An ISqlFactory interface and classes that implemented it for dealing with different flavors of SQL.
* Builtin support for SQL Server, Oracle Managed, Oracle Native and MySql
* Easily extensible for other database providers.

## Code Example
(See samples/SampleNetCore)
```c#
using Marsonsoft.Database;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;
using System.IO;

namespace SampleNetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            // Build up a configuration from our json file
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json", optional: true)
                .Build();

            // Get a DatabaseConfiguration from our configuration
            var databaseConfiguration = configuration.Get<Marsonsoft.Database.Configuration.DatabaseConfiguration>();

            // Create a DataEnvironmentProvider for the DatabaseConfiguration
            var dataEnvironmentProvider = DataEnvironmentProviderFactory.Create(databaseConfiguration);

            // Get the default IDataEnvironment for our configuration
            var dataEnvironment = dataEnvironmentProvider.GetDefault();

            // Start using the dataEnvironment
            using (var connection = dataEnvironment.ConnectionProvider.OpenConnection())
            {
                // Use the IDbConnection to create the IDbCommand
                using (var command = connection.CreateCommand("select id, name from customers where order_type = :type"))
                {
                    // Extension method to add a parameter
                    command.AddParameter("order_type", "good");
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Use extension methods to get typed + named values from the reader
                            Debug.WriteLine($"id: {reader.GetValue<long>("id")}, name: {reader.GetValue<string>("name")}");
                        }
                    }
                }
            }

        }
    }
}
```

The appSettings.json file for this example:
```json
{
  "defaultName": "defaultConnection",
  "connections": [
    {
      "name": "defaultConnection",
      "providerName": "System.Data.SqlClient",
      "connectionString": "server=myServerAddress;database=myDataBase;user id=myUsername;password=myPassword"
    }
  ],
  "providers": [
    {
      "invariantName": "System.Data.SqlClient",
      "assemblyQualifiedName": "System.Data.SqlClient.SqlClientFactory, System.Data.SqlClient, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    }
  ]
}
```

## License

The project is licensed under the MIT license.