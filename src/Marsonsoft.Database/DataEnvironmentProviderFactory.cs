﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Responsible for creating <see cref="IDataEnvironmentProvider"/> objects.
    /// </summary>
    public static class DataEnvironmentProviderFactory
    {
        /// <summary>
        /// Creates an instance of a class implementing the <see cref="IDataEnvironmentProvider"/> interface using the
        /// specified database configuration.
        /// </summary>
        /// <param name="databaseConfiguration">The database configuration to use.</param>
        /// <returns>An instance of a class implementing the <see cref="IDataEnvironmentProvider"/> interface</returns>
        /// <exception cref="ArgumentNullException"><paramref name="databaseConfiguration"/></exception>
        /// <exception cref="ArgumentException">Database configuration is invalid.</exception>
        public static IDataEnvironmentProvider Create(Configuration.DatabaseConfiguration databaseConfiguration)
        {
            if (databaseConfiguration == null)
            {
                throw new ArgumentNullException(nameof(databaseConfiguration));
            }

            if (databaseConfiguration.Providers == null || databaseConfiguration.Providers.Count == 0)
            {
                throw new ArgumentException("At least one provider must be specified.", nameof(databaseConfiguration));
            }

            return InternalCreate(databaseConfiguration, new DbProviderFactories(databaseConfiguration.Providers));
        }

        /// <summary>
        /// Creates an instance of a class implementing the <see cref="IDataEnvironmentProvider"/> interface using the
        /// specified database configuration and DB provider factory factory.
        /// </summary>
        /// <param name="databaseConfiguration">The database configuration to use.</param>
        /// <param name="dbProviderFactories">The DB provider factory factory to use.</param>
        /// <returns>An instance of a class implementing the <see cref="IDataEnvironmentProvider"/> interface</returns>
        /// <remarks>
        /// The <see cref="Configuration.Provider"/> part of the <paramref name="databaseConfiguration"/> is ignored in
        /// this method (although the class behind <paramref name="dbProviderFactories"/> may actually be using it).
        /// </remarks>
        public static DataEnvironmentProvider Create(Configuration.DatabaseConfiguration databaseConfiguration, IDbProviderFactoryFactory dbProviderFactories)
        {
            if (databaseConfiguration == null)
            {
                throw new ArgumentNullException(nameof(databaseConfiguration));
            }

            return InternalCreate(databaseConfiguration, dbProviderFactories);
        }

        private static DataEnvironmentProvider InternalCreate(Configuration.DatabaseConfiguration databaseConfiguration, IDbProviderFactoryFactory dbProviderFactories)
        {
            var defaultDataEnvironmentProvider = new DataEnvironmentProvider(databaseConfiguration, dbProviderFactories);

            var dataEnvironmentTypes = GetAllTypesImplementingIDataEnvironment();
            foreach (var dataEnvironmentType in dataEnvironmentTypes)
            {
                var providerNames = DataEnvironmentProvider.GetAllProviderNamesSupportedBy(dataEnvironmentType);
                foreach (var providerName in providerNames)
                {
                    defaultDataEnvironmentProvider.Add(providerName, dataEnvironmentType);
                }
            }

            return defaultDataEnvironmentProvider;
        }

        private static IEnumerable<Type> GetAllTypesImplementingIDataEnvironment()
        {
            return from type in Assembly.GetExecutingAssembly().GetTypes()
                   where type.IsClass && !type.IsAbstract && type.GetInterfaces().Contains(typeof(IDataEnvironment))
                   select type;
        }

    }
}
