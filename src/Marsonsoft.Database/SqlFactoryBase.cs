﻿using System;
using System.Globalization;
using System.Linq;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Represents the ability to create SQL elements. Provides a base class for classes that implement
    /// database-specific SQL dialects.
    /// </summary>
    public abstract class SqlFactoryBase : ISqlFactory
    {
        /// <summary>
        /// Gets or sets a value controlling if and how identifiers created by the class implementing this interface
        /// are quoted.
        /// </summary>
        public virtual IdentifierQuoting IdentifierQuoting { get; set; }

        /// <summary>
        /// Returns the specified name as an identifier based on the rules set in <see cref="IdentifierQuoting"/>.
        /// </summary>
        /// <remarks>
        /// Examples:
        /// <p>customer_id => "customer_id"   (Oracle)</p>
        /// <p>customer_id => `customer_id`   (MySQL)</p>
        /// <p>customer_id => [customer_id]   (SQL Server)</p>
        /// </remarks>
        /// <param name="name">The name to represent as an SQL identifier.</param>
        /// <returns>The <paramref name="name"/> as an SQL identifier.</returns>
        public virtual string Identifier(string name)
        {
            bool quote = false;

            switch (IdentifierQuoting)
            {
                case IdentifierQuoting.OnlyWhenNeeded:
                    quote = IdentifierNeedsQuoting(name);
                    break;
                case IdentifierQuoting.Never:
                    quote = false;
                    break;
                case IdentifierQuoting.Always:
                    quote = true;
                    break;
            }

            if (quote)
            {
                return QuoteIdentifier(name);
            }
            else
            {
                return name;
            }
        }

        /// <summary>
        /// Returns the specified tble name as an identifier based on the rules set in <see cref="IdentifierQuoting"/>.
        /// </summary>
        /// <remarks>
        /// Examples (when <see cref="IdentifierQuoting"/> is <see cref="IdentifierQuoting.Always"/>):
        /// <p>my_schema.my_table => "my_schema"."my_table"   (Oracle)</p>
        /// <p>my_schema.my_table => `my_schema`.`my_table`   (MySQL)</p>
        /// <p>my_schema.my_table => [my_schema].[my_table]   (SQL Server)</p>
        /// </remarks>
        /// <param name="tableName">The table name to represent as an SQL identifier.</param>
        /// <returns>The <paramref name="tableName"/> as an SQL identifier.</returns>
        public virtual string Identifier(TableName tableName)
        {
            if (tableName.Schema != null)
            {
                return Identifier(tableName.Schema) + "." + Identifier(tableName.Name);
            }
            else
            {
                return Identifier(tableName.Name);
            }
        }

        /// <summary>
        /// Returns a valid SQL literal representation of the specified value.
        /// </summary>
        /// <param name="value">The value to get a literal for.</param>
        /// <returns>A valid SQL literal.</returns>
        public virtual string Literal(object value)
        {
            if (value == null || value is DBNull)
            {
                return "NULL";
            }

            string stringValue = value as string;
            if (stringValue != null) return StringToSql(stringValue);
            if (value is short) return NumericToSql((short)value);
            if (value is ushort) return NumericToSql((ushort)value);
            if (value is int) return NumericToSql((int)value);
            if (value is uint) return NumericToSql((uint)value);
            if (value is long) return NumericToSql((long)value);
            if (value is ulong) return NumericToSql((ulong)value);
            if (value is decimal) return NumericToSql((decimal)value);
            if (value is float) return NumericToSql((float)value);
            if (value is double) return NumericToSql((double)value);
            if (value is DateTime) return ConditionalDateTimeToSql((DateTime)value);
            return value.ToString();
        }

        /// <summary>
        /// Converts the specified <paramref name="numericValue"/> to a SQL literal.
        /// </summary>
        /// <typeparam name="T">The type of the <paramref name="numericValue"/>.</typeparam>
        /// <param name="numericValue">The numeric value to convert.</param>
        /// <returns>The numeric value as an SQL literal.</returns>
        public virtual string NumericToSql<T>(T numericValue)
            where T : IConvertible
        {
            return numericValue.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the specified <paramref name="value"/> to an SQL date or timestamp literal.
        /// </summary>
        /// <param name="value">The date or date/time value.</param>
        /// <returns>An SQL date literal if value contains only a date value, otherwise an SQL timestamp literal.</returns>
        public virtual string ConditionalDateTimeToSql(DateTime value)
        {
            var date = value.Date;
            if (value == date)
            {
                return DateToSql(date);
            }
            else
            {
                return DateTimeToSql(value);
            }
        }

        /// <summary>
        /// Converts the specified <paramref name="value"/> to an SQL date literal.
        /// </summary>
        /// <param name="value">The date value.</param>
        /// <returns>An SQL date literal.</returns>
        public virtual string DateToSql(DateTime value)
        {
            return "DATE '" + value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) + "'";
        }

        /// <summary>
        /// Converts the specified <paramref name="value"/> to an SQL timestamp literal.
        /// </summary>
        /// <param name="value">The date/time value.</param>
        /// <returns>An SQL timestamp literal.</returns>
        public virtual string DateTimeToSql(DateTime value)
        {
            return "TIMESTAMP '" + value.ToString("yyyy-MM-dd HH:mm:ss.fffff", CultureInfo.InvariantCulture) + "'";
        }

        /// <summary>
        /// Converts the specified <paramref name="value"/> to an SQL string literal.
        /// </summary>
        /// <param name="value">The string value to convert.</param>
        /// <returns>An SQL string literal.</returns>
        public virtual string StringToSql(string value)
        {
            if (value.Any(c => c <= 1))
            {
                throw new ArgumentException("Value contains invalid characters.");
            }

            return "'" + value.Replace("'", "''") + "'";
        }

        /// <summary>
        /// Determines if the specified name, when used as an identifier in SQL, needs to be quoted.
        /// </summary>
        /// <param name="name">The name to check.</param>
        /// <returns><c>true</c> if the name needs to be quoted, <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">name</exception>
        protected abstract bool IdentifierNeedsQuoting(string name);

        /// <summary>
        /// Returnes the specified name as a quoted identifier.
        /// </summary>
        /// <param name="name">The name to quote.</param>
        /// <returns><paramref name="name"/> as a quoted identifier.</returns>
        /// <exception cref="ArgumentNullException">name</exception>
        protected abstract string QuoteIdentifier(string name);
    }
}
