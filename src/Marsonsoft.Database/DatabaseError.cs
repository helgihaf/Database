﻿namespace Marsonsoft.Database
{
    /// <summary>
    /// Enumerates well-known, database agnostic error conditions.
    /// </summary>
    public enum DatabaseError
    {
        /// <summary>
        /// Unknown error. Even if well known.
        /// </summary>
        Unknown,

        /// <summary>
        /// Cannot connect to the database server because user name and/or password are invalid, or the user is not authorized to connect to the dateabase. Used for ORA-1017 and MySQL 1045.
        /// </summary>
        LoginFailure,

        /// <summary>
        /// Server name or information provided to identify the database server and/or database is invalid. Used for ORA-12154 and MySQL 1042, 1049.
        /// </summary>
        InvalidDatabase,

        /// <summary>
        /// Generic error in SQL statement or procedure. Used for ORA-604.
        /// </summary>
        GenericSQLError,
    }
}
