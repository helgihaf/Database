﻿using System.Data;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Contains extension methods for the <see cref="IDbCommand"/> class.
    /// </summary>
    public static class DbCommandExtensions
    {
        /// <summary>
        /// Creates and adds a parameter to the specified command.
        /// </summary>
        /// <param name="command">The command to add the parameter to.</param>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <param name="dbType">The <see cref="DbType"/> of the parameter.</param>
        /// <returns>A <see cref="IDbDataParameter"/> object that was initialized with the specified parameters and has already been added to the <paramref name="command"/>.</returns>
        public static IDbDataParameter AddParameter(this IDbCommand command, string parameterName, DbType dbType)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.DbType = dbType;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Creates and adds a parameter to the specified command.
        /// </summary>
        /// <param name="command">The command to add the parameter to.</param>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <param name="obj">The value of the parameter.</param>
        /// <returns>A <see cref="IDbDataParameter"/> object that was initialized with the specified parameters and has already been added to the <paramref name="command"/>.</returns>
        public static IDbDataParameter AddParameter(this IDbCommand command, string parameterName, object obj)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.Value = obj;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Creates and adds a parameter to the specified command.
        /// </summary>
        /// <param name="command">The command to add the parameter to.</param>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <param name="dbType">The <see cref="DbType"/> of the parameter.</param>
        /// <param name="direction">The <see cref="ParameterDirection"/> of the parameter.</param>
        /// <returns>A <see cref="IDbDataParameter"/> object that was initialized with the specified parameters and has already been added to the <paramref name="command"/>.</returns>
        public static IDbDataParameter AddParameter(this IDbCommand command, string parameterName, DbType dbType, ParameterDirection direction)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.DbType = dbType;
            parameter.Direction = direction;
            command.Parameters.Add(parameter);
            return parameter;
        }

        /// <summary>
        /// Creates and adds a parameter to the specified command.
        /// </summary>
        /// <param name="command">The command to add the parameter to.</param>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <param name="dbType">The <see cref="DbType"/> of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        /// <param name="direction">The <see cref="ParameterDirection"/> of the parameter.</param>
        /// <returns>A <see cref="IDbDataParameter"/> object that was initialized with the specified parameters and has already been added to the <paramref name="command"/>.</returns>
        public static IDbDataParameter AddParameter(this IDbCommand command, string parameterName, DbType dbType, object value, ParameterDirection direction)
        {
            var parameter = command.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.DbType = dbType;
            parameter.Value = value;
            parameter.Direction = direction;
            command.Parameters.Add(parameter);
            return parameter;
        }
    }
}
