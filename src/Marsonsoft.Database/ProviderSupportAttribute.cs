﻿using System;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Custom attribute to declare which data providers are supported by a class implementing <see cref="IDataEnvironment"/>./>
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public sealed class ProviderSupportAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProviderSupportAttribute"/> class.
        /// </summary>
        /// <param name="providerName">The provider name supported.</param>
        public ProviderSupportAttribute(string providerName)
        {
            ProviderName = providerName;
        }

        /// <summary>
        /// Gets the provider invariant name supported.
        /// </summary>
        public string ProviderName { get; }
    }
}
