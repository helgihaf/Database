﻿namespace Marsonsoft.Database
{
    /// <summary>
    /// Interface for checking validity of strings as valid database identifiers.
    /// </summary>
    public interface ITextValidator
    {
        /// <summary>
        /// Checks if the specified user name is a valid user name identifier.
        /// </summary>
        /// <param name="userName">The user name to check.</param>
        /// <returns><c>true</c> if <paramref name="userName"/> is a valid user name, <c>false</c> otherwise.</returns>
        bool IsValidUserName(string userName);

        /// <summary>
        /// Checks if the specified password is a valid password identifier.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <returns><c>true</c> if <paramref name="password"/> is a valid password, <c>false</c> otherwise.</returns>
        bool IsValidPassword(string password);

        /// <summary>
        /// Checks if the specified user name is a valid identifier.
        /// </summary>
        /// <param name="identifier">The identifier to check.</param>
        /// <returns><c>true</c> if <paramref name="identifier"/> is a valid Oracle identifier, <c>false</c> otherwise.</returns>
        bool IsValidIdentifier(string identifier);
    }
}
