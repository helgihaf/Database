﻿using System;
using System.Linq;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Represents an ANSI 92 SQL implementation of the <see cref="ISqlFactory"/> interface.
    /// </summary>
    /// <seealso cref="Marsonsoft.Database.SqlFactoryBase" />
    public class SqlFactoryAnsi92 : SqlFactoryBase
    {
        /// <summary>
        /// Determines if the specified name, when used as an identifier in SQL, needs to be quoted.
        /// </summary>
        /// <param name="name">The name to check.</param>
        /// <returns><c>true</c> if the name needs to be quoted, <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">name</exception>
        protected override bool IdentifierNeedsQuoting(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return name.Any(c => char.IsWhiteSpace(c) || c == '\'');
        }

        /// <summary>
        /// Returnes the specified name as a quoted identifier.
        /// </summary>
        /// <param name="name">The name to quote.</param>
        /// <returns><paramref name="name"/> as a quoted identifier.</returns>
        /// <exception cref="ArgumentNullException">name</exception>
        protected override string QuoteIdentifier(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return "\"" + name + "\"";
        }
    }
}
