﻿using System.Data.Common;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Factory for getting <see cref="DbProviderFactory"/> instances.
    /// </summary>
    /// <remarks>
    /// The function of this interface is implemented in .NET Framework in the System.Data.Common.DbProviderFactories class
    /// and is done similarly in a config-agnostic way in <see cref="DbProviderFactories"/> class.
    /// </remarks>
    public interface IDbProviderFactoryFactory
    {
        /// <summary>
        /// Gets a <see cref="DbProviderFactory"/> with the specified provider invariant name.
        /// </summary>
        /// <param name="providerInvariantName">The DB provider invariant name.</param>
        /// <returns>An instance inheriting from <see cref="DbProviderFactory"/>.</returns>
        DbProviderFactory GetFactory(string providerInvariantName);
    }
}
