﻿using System;
using System.Data;
using System.Data.Common;

namespace Marsonsoft.Database
{
    /// <summary>
    /// A basic implementation of the <see cref="IConnectionProvider"/> interface.
    /// </summary>
    public class ConnectionProvider : IConnectionProvider
    {
        private readonly string connectionString;
        private readonly DbProviderFactory dbProviderFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionProvider"/> class with the specified arguments.
        /// </summary>
        /// <param name="dbProviderFactory">The provider factory object to use.</param>
        /// <param name="connectionString">The connection string to use.</param>
        public ConnectionProvider(DbProviderFactory dbProviderFactory, string connectionString)
        {
            this.dbProviderFactory = dbProviderFactory ?? throw new ArgumentNullException(nameof(dbProviderFactory));
            this.connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
        }

        /// <summary>
        /// Occurs when a connection has been opened.
        /// </summary>
        public event EventHandler<ConnectionEventArgs> ConnectionOpened;

        /// <summary>
        /// Creates a new connection with this provider's connection string set. The connection is not open.
        /// </summary>
        /// <returns>A new instance implementing <see cref="System.Data.IDbConnection"/>.</returns>
        public virtual IDbConnection CreateConnection()
        {
            var connection = dbProviderFactory.CreateConnection();
            connection.ConnectionString = connectionString;

            return connection;
        }

        /// <summary>
        /// Creates and opens a new connection using this provider's connection string.
        /// </summary>
        /// <returns>An instance of <see cref="System.Data.IDbConnection"/>.</returns>
        public IDbConnection OpenConnection()
        {
            var connection = CreateConnection();
            try
            {
                connection.Open();
            }
            catch
            {
                connection.Dispose();
                throw;
            }

            OnConnectionOpened(connection);
            return connection;
        }

        /// <summary>
        /// Raises the <see cref="ConnectionOpened"/> event.
        /// </summary>
        /// <param name="connection">The connection that was opened.</param>
        protected virtual void OnConnectionOpened(IDbConnection connection)
        {
            ConnectionOpened?.Invoke(this, new ConnectionEventArgs { Connection = connection });
        }
    }
}
