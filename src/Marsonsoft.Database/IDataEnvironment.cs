﻿using System.Data.Common;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Represents an environment for working with database objects.
    /// </summary>
    public interface IDataEnvironment
    {
        /// <summary>
        /// Gets the <see cref="IConnectionProvider"/> instance of this data environment.
        /// </summary>
        IConnectionProvider ConnectionProvider { get; }

        /// <summary>
        /// Gets the <see cref="DbProviderFactory"/> instance of this data environment.
        /// </summary>
        DbProviderFactory DbProviderFactory { get; }

        /// <summary>
        /// Gets the <see cref="ITextValidator"/> instance of this data environment.
        /// </summary>
        ITextValidator TextValidator { get; }

        /// <summary>
        /// Gets the <see cref="ISqlFactory"/> instance of this data environment.
        /// </summary>
        ISqlFactory Sql { get; }
    }
}
