﻿using System.Data;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Contains extension methods for the <see cref="IDbConnection"/> interface.
    /// </summary>
    public static class DbConnectionExtensions
    {
        /// <summary>
        /// Creates an object implementing <see cref="IDbCommand"/>, with the specified text.
        /// </summary>
        /// <param name="connection">The connection to which the command belongs.</param>
        /// <param name="cmdText">The command text.</param>
        /// <returns>A new instance of the <see cref="IDbCommand"/> class.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static IDbCommand CreateCommand(this IDbConnection connection, string cmdText)
        {
            var command = connection.CreateCommand();
            command.CommandText = cmdText;
            return command;
        }
    }
}
