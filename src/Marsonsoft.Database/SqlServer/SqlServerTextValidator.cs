﻿namespace Marsonsoft.Database.SqlServer
{
    /// <summary>
    /// This class checks the validity of strings as quoted Microsoft SQL Server identifiers.
    /// </summary>
    public class SqlServerTextValidator : ITextValidator
    {
        // https://docs.microsoft.com/en-us/sql/relational-databases/databases/database-identifiers

        private const int MaxIdentifierLength = 128;

        private static readonly char[] invalidChars = new[] { '\0', '\u0001' };

        /// <summary>
        /// Checks if the specified user name is a valid Microsoft SQL Server user name identifier.
        /// </summary>
        /// <param name="userName">The user name to check.</param>
        /// <returns><c>true</c> if userName is a valid Oracle user name when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidUserName(string userName)
        {
            return
                IsValidIdentifier(userName)
                &&
                userName.Length <= MaxIdentifierLength;
        }

        /// <summary>
        /// Checks if the specified password is a valid Microsoft SQL Server password identifier.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <returns><c>true</c> if password is a valid Oracle password when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidPassword(string password)
        {
            return IsValidIdentifier(password);
        }

        /// <summary>
        /// Checks if the specified user name is a valid Microsoft SQL Server identifier.
        /// </summary>
        /// <param name="identifier">The identifier to check.</param>
        /// <returns><c>true</c> if identifier is a valid Microsoft SQL Server identifier when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidIdentifier(string identifier)
        {
            return
                !string.IsNullOrEmpty(identifier)
                &&
                identifier.Length <= MaxIdentifierLength
                &&
                identifier.IndexOfAny(invalidChars) == -1;
        }
    }
}
