﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Provides objects implementing <see cref="IDataEnvironment"/>.
    /// </summary>
    public class DataEnvironmentProvider : IDataEnvironmentProvider
    {
        private readonly IDbProviderFactoryFactory dbProviderFactories;
        private readonly Dictionary<string, Type> dataEnvironmentDictionary = new Dictionary<string, Type>();

        /// <summary>
        /// Initializes a new instance of the <see cref="DataEnvironmentProvider"/> class.
        /// </summary>
        /// <param name="configuration">The database configuration to use.</param>
        /// <param name="dbProviderFactories">The DB provider factory factory to use.</param>
        /// <exception cref="System.ArgumentNullException">infoProvider</exception>
        protected internal DataEnvironmentProvider(Configuration.DatabaseConfiguration configuration, IDbProviderFactoryFactory dbProviderFactories)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            this.dbProviderFactories = dbProviderFactories ?? throw new ArgumentNullException(nameof(dbProviderFactories));
        }

        /// <summary>
        /// Gets the database configuration.
        /// </summary>
        public Configuration.DatabaseConfiguration Configuration { get; }

        /// <summary>
        /// Adds an environment type to the list of types provided. The <see cref="DbProviderFactory"/> types supported are
        /// automatically detected by looking for the <see cref="ProviderSupportAttribute"/> attributes of the type.
        /// </summary>
        /// <param name="type">The environment type, implementing <see cref="IDataEnvironment"/>, that should be added.</param>
        public void Add(Type type)
        {
            ValidateTypeToAdd(type);
            var providerNames = GetAllProviderNamesSupportedBy(type);
            if (!providerNames.Any())
            {
                throw new ArgumentException($"Type {type.FullName} has no {nameof(ProviderSupportAttribute)}", nameof(type));
            }
            foreach (var providerName in providerNames)
            {
                dataEnvironmentDictionary.Add(providerName, type);
            }
        }

        /// <summary>
        /// Adds an environment type to the list of types provided along with the provider invariant name of the <see cref="DbProviderFactory"/>
        /// it supports.
        /// </summary>
        /// <param name="providerInvariantName">Name of the data provider (<see cref="DbProviderFactory"/>) to that <paramref name="type"/> supports.</param>
        /// <param name="type">The environment type, implementing <see cref="IDataEnvironment"/>, that should be added.</param>
        public void Add(string providerInvariantName, Type type)
        {
            ValidateTypeToAdd(type);
            dataEnvironmentDictionary.Add(providerInvariantName, type);
        }

        /// <summary>
        /// Gets an environment for the specified data provider and connection string.
        /// </summary>
        /// <param name="providerInvariantName">Name of the data provider (<see cref="DbProviderFactory"/>) to use.</param>
        /// <param name="connectionString">The connection string to use.</param>
        /// <returns>An object implementing <see cref="IDataEnvironment"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="connectionString"/> or <paramref name="connectionString"/></exception>
        public IDataEnvironment GetByConnectionSetting(string providerInvariantName, string connectionString)
        {
            if (providerInvariantName == null)
            {
                throw new ArgumentNullException(nameof(providerInvariantName));
            }

            if (connectionString == null)
            {
                throw new ArgumentNullException(nameof(connectionString));
            }

            return CreateDataEnvironment(providerInvariantName, connectionString);
        }

        /// <summary>
        /// Gets the default <see cref="IDataEnvironment"/>.
        /// </summary>
        /// <returns>An object implementing <see cref="IDataEnvironment"/>.</returns>
        /// <exception cref="InvalidOperationException">No default data provider name was found.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public IDataEnvironment GetDefault()
        {
            if (!(Configuration.Connections?.Count > 0))
            {
                throw new InvalidOperationException("No connections are defined in configuration.");
            }

            Configuration.Connection defaultConnection = Configuration.Connections.FirstOrDefault(p => p.Name == Configuration.DefaultName);
            string providerName = ProviderNameOrDefault(defaultConnection);

            return CreateDataEnvironment(providerName, defaultConnection.ConnectionString);
        }

        /// <summary>
        /// Gets an environment for the specified connection string name.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>An object implementing <see cref="IDataEnvironment"/>.</returns>
        /// <remarks>
        /// The connection string settings identified by <paramref name="name"/> will be used to set the <see cref="IDataEnvironment.DbProviderFactory"/> of the
        /// resulting environment and to set the default connection string used by <see cref="IDataEnvironment.ConnectionProvider"/>. If the named
        /// connection string does not have a provider specified, the <see cref="Configuration.DatabaseConfiguration.DefaultProviderName"/> will be used.
        /// </remarks>
        /// <exception cref="System.ArgumentException">A connection string with the value specified in <paramref name="name"/> was not found.</exception>
        public IDataEnvironment GetByConnectionStringName(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            Configuration.Connection connection = Configuration.Connections.FirstOrDefault(p => p.Name == name);
            if (connection == null)
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "No connection string with name '{0}' found.", name), nameof(name));
            }

            string providerName = ProviderNameOrDefault(connection);

            return CreateDataEnvironment(providerName, connection.ConnectionString);
        }

        internal static IEnumerable<string> GetAllProviderNamesSupportedBy(Type dataEnvironmentType)
        {
            return
                from attribute in dataEnvironmentType.GetCustomAttributes<ProviderSupportAttribute>(true)
                select attribute.ProviderName;
        }

        private static void ValidateTypeToAdd(Type type)
        {
            if (!typeof(IDataEnvironment).IsAssignableFrom(type))
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Type must be assignable to {0}.", nameof(IDataEnvironment)), nameof(type));
            }
        }

        private IDataEnvironment CreateDataEnvironment(string providerInvariantName, string connectionString)
        {
            if (!dataEnvironmentDictionary.TryGetValue(providerInvariantName, out Type dataEnvironmentType))
            {
                throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "No type implementing {0} was found supporting provider {1}.", nameof(IDataEnvironment), providerInvariantName), nameof(providerInvariantName));
            }

            DbProviderFactory dbProviderFactory = dbProviderFactories.GetFactory(providerInvariantName);
            if (dbProviderFactory == null)
            {
                throw new ArgumentException($"No DbProvider with invariant name {providerInvariantName} was found.");
            }

            IConnectionProvider connectionProvider = new ConnectionProvider(dbProviderFactory, connectionString);

            return Activator.CreateInstance(dataEnvironmentType, dbProviderFactory, connectionProvider) as IDataEnvironment;
        }

        private string ProviderNameOrDefault(Configuration.Connection connection)
        {
            string providerName = connection?.ProviderName ?? Configuration.DefaultProviderName;
            if (string.IsNullOrEmpty(providerName))
            {
                throw new InvalidOperationException("No default provider name was found in configuration.");
            }

            return providerName;
        }
    }
}
