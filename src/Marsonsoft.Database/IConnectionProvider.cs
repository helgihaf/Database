﻿using System;
using System.Data;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Represents a provider of <see cref="IDbConnection"/> objects.
    /// </summary>
    public interface IConnectionProvider
    {
        /// <summary>
        /// Occurs when a connection has been opened.
        /// </summary>
        event EventHandler<ConnectionEventArgs> ConnectionOpened;

        /// <summary>
        /// Creates and open a new object implementing <see cref="IDbConnection"/>, based on the default connection string for this provider.
        /// </summary>
        /// <returns>An instance of <see cref="System.Data.IDbConnection"/>.</returns>
        IDbConnection OpenConnection();

        /// <summary>
        /// Creates a new uninitialized object implementing <see cref="IDbConnection"/>.
        /// </summary>
        /// <returns>A new instance of <see cref="System.Data.IDbConnection"/>.</returns>
        IDbConnection CreateConnection();
    }
}
