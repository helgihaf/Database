﻿using System;
using System.Data;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Contains event data for <see cref="IDbConnection"/> values.
    /// </summary>
    public class ConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// Gets or sets the connection of this event data.
        /// </summary>
        public IDbConnection Connection { get; set; }
    }
}