﻿namespace Marsonsoft.Database
{
    /// <summary>
    /// Determines how identifiers are quoted.
    /// </summary>
    public enum IdentifierQuoting
    {
        /// <summary>
        /// Only quote identifiers when syntax rules require it, for example when identifiers include spaces.
        /// </summary>
        OnlyWhenNeeded,

        /// <summary>
        /// Never quote identifiers.
        /// </summary>
        Never,

        /// <summary>
        /// Always quote identifiers.
        /// </summary>
        Always,
    }
}
