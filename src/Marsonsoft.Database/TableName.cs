﻿using System;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Represents a table name in an optional schema.
    /// </summary>
    public struct TableName
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TableName"/> struct using a table name without a schema.
        /// </summary>
        /// <param name="name">The table name.</param>
        public TableName(string name)
            : this(null, name)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TableName"/> struct using a table name and a schema name.
        /// </summary>
        /// <param name="schema">The schema name, can be null.</param>
        /// <param name="name">The table name.</param>
        /// <exception cref="System.ArgumentNullException">name</exception>
        public TableName(string schema, string name)
        {
            Schema = schema;
            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        /// <summary>
        /// Gets the schema name.
        /// </summary>
        public string Schema { get; private set; }

        /// <summary>
        /// Gets the table name (without the schema name).
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Determines whether two specified <see cref="TableName" /> structs have the same value.
        /// </summary>
        /// <param name="a">The first <see cref="TableName"/> to compare.</param>
        /// <param name="b">The second <see cref="TableName"/> to compare.</param>
        /// <returns><c>true</c> if the value of a is the same as the value of b; otherwise, <c>false</c>.</returns>
        public static bool operator ==(TableName a, TableName b)
        {
            return a.Equals(b);
        }

        /// <summary>
        /// Determines whether two specified <see cref="TableName" /> structs have different values.
        /// </summary>
        /// <param name="a">The first <see cref="TableName"/> to compare.</param>
        /// <param name="b">The second <see cref="TableName"/> to compare.</param>
        /// <returns><c>true</c> if the value of a is different from the value of b; otherwise, <c>false</c>.</returns>
        public static bool operator !=(TableName a, TableName b)
        {
            return !a.Equals(b);
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Schema))
                return Schema + "." + Name;
            else
                return Name;
        }

        /// <summary>
        /// Returns a lower-case invariant version of this instance.
        /// </summary>
        /// <returns>A <see cref="TableName"/> which is a lower-case invariant version of this instance.</returns>
        public TableName ToLowerInvariant()
        {
            string schema = Schema;
            if (schema != null)
                schema = schema.ToLowerInvariant();
            return new TableName(schema, Name.ToLowerInvariant());
        }

        /// <summary>
        /// Returns an upper-case invariant version of this instance.
        /// </summary>
        /// <returns>A <see cref="TableName"/> which is an upper-case invariant version of this instance.</returns>
        public TableName ToUpperInvariant()
        {
            string schema = Schema;
            if (schema != null)
                schema = schema.ToUpperInvariant();
            return new TableName(schema, Name.ToUpperInvariant());
        }

        /// <summary>
        /// Determines whether the specified <see cref="object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (!(obj is TableName))
                return false;

            return Equals((TableName)obj);
        }

        /// <summary>
        /// Determines whether the specified <see cref="TableName" /> is equal to this instance.
        /// </summary>
        /// <param name="other">The <see cref="TableName" /> to compare with this instance.</param>
        /// <returns>
        /// <c>true</c> if the specified <see cref="TableName" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public bool Equals(TableName other)
        {
            return Schema == other.Schema && Name == other.Name;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return (Schema != null ? Schema.GetHashCode() : 0) ^ Name.GetHashCode();
        }
    }
}
