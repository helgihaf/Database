﻿using System.Data.Common;

namespace Marsonsoft.Database.MySql
{
    /// <summary>
    /// Contains MySQL-specific extension methods for the <see cref="DbException"/> class.
    /// </summary>
    public static class DbExceptionExtensions
    {
        /// <summary>
        /// Gets a platform-agnostic <see cref="DatabaseError"/> value from the DbException.
        /// </summary>
        /// <param name="exception">In instance of an MySQL-specific exception class inheriting from <see cref="DbException"/>.</param>
        /// <returns>A <see cref="DatabaseError"/> value.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static DatabaseError GetDatabaseError(this DbException exception)
        {
            DatabaseError result = DatabaseError.Unknown;
            if (exception.GetType().FullName.StartsWith("MySql.", System.StringComparison.Ordinal))
            {
                int number = ((dynamic)exception).Number;
                if (number != 0)
                {
                    result = MySqlExceptionNumber.ToDatabaseError(((dynamic)exception).Number);
                }
                else
                {
                    var inner = exception as DbException;
                    if (inner != null)
                    {
                        result = inner.GetDatabaseError();
                    }
                }
            }

            return result;
        }
    }
}
