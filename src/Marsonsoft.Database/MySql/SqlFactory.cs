﻿using System;

namespace Marsonsoft.Database.MySql
{
    /// <summary>
    /// Implements the <see cref="ISqlFactory"/> interface for the SQL dialect used by MySQL.
    /// </summary>
    public class SqlFactory : SqlFactoryAnsi92
    {
        /// <summary>
        /// Returnes the specified name as a quoted identifier.
        /// </summary>
        /// <param name="name">The name to quote.</param>
        /// <returns><paramref name="name"/> as a quoted identifier.</returns>
        /// <exception cref="ArgumentNullException">name</exception>
        protected override string QuoteIdentifier(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return "`" + name + "`";
        }
    }
}
