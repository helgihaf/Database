﻿namespace Marsonsoft.Database.MySql
{
    /// <summary>
    /// This class checks the validity of strings as quoted MySQL identifiers.
    /// </summary>
    public class MySqlTextValidator : ITextValidator
    {
        private static readonly char[] invalidChars = new[] { '`', '\0', '\u0001' };

        // https://dev.mysql.com/doc/refman/5.7/en/identifiers.html

        /// <summary>
        /// Checks if the specified user name is a valid MySQL user name identifier.
        /// </summary>
        /// <param name="userName">The user name to check.</param>
        /// <returns><c>true</c> if userName is a valid Oracle user name when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidUserName(string userName)
        {
            return
                IsValidIdentifier(userName)
                &&
                userName.Length <= 32;
        }

        /// <summary>
        /// Checks if the specified password is a valid MySQL password identifier.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <returns><c>true</c> if password is a valid Oracle password when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidPassword(string password)
        {
            return IsValidIdentifier(password);
        }

        /// <summary>
        /// Checks if the specified user name is a valid MySQL identifier.
        /// </summary>
        /// <param name="identifier">The identifier to check.</param>
        /// <returns><c>true</c> if identifier is a valid MySQL identifier when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidIdentifier(string identifier)
        {
            return
                !string.IsNullOrEmpty(identifier)
                &&
                identifier.Length <= 63
                &&
                identifier.IndexOfAny(invalidChars) == -1;
        }
    }
}
