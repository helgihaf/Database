﻿namespace Marsonsoft.Database.MySql
{
    internal static class MySqlExceptionNumber
    {
        public const int UnableToConnect = 1042;
        public const int AccessDeniedForUser = 1045;
        public const int UnknownDatabase = 1049;

        public static DatabaseError ToDatabaseError(int number)
        {
            switch (number)
            {
                case AccessDeniedForUser:
                    return DatabaseError.LoginFailure;
                case UnableToConnect:
                case UnknownDatabase:
                    return DatabaseError.InvalidDatabase;
            }

            return DatabaseError.Unknown;
        }
    }
}
