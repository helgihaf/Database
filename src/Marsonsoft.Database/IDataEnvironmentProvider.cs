﻿using System;
using Marsonsoft.Database.Configuration;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Provides objects implementing <see cref="IDataEnvironment"/>.
    /// </summary>
    public interface IDataEnvironmentProvider
    {
        /// <summary>
        /// Gets the database configuration used to create this provider.
        /// </summary>
        DatabaseConfiguration Configuration { get; }

        /// <summary>
        /// Adds a type that implements <see cref="IDataEnvironment"/> to the list of types provided along with the provider invariant
        /// name of the <see cref="System.Data.Common.DbProviderFactory"/> it supports.
        /// </summary>
        /// <param name="providerInvariantName">Name of the data provider (<see cref="System.Data.Common.DbProviderFactory"/>) to that <paramref name="type"/> supports.</param>
        /// <param name="type">The environment type, implementing <see cref="IDataEnvironment"/>, that should be added.</param>
        void Add(string providerInvariantName, Type type);

        /// <summary>
        /// Adds a type that implements <see cref="IDataEnvironment"/> to the list of types provided by this provider.
        /// </summary>
        /// <param name="type">The environment type, implementing <see cref="IDataEnvironment"/>, that should be added.</param>
        /// <remarks>
        /// The <see cref="System.Data.Common.DbProviderFactory"/> types supported are automatically detected by looking for the
        /// <see cref="ProviderSupportAttribute"/> attributes of the <paramref name="type"/>.
        /// </remarks>
        void Add(Type type);

        /// <summary>
        /// Gets an environment for the specified data provider and connection string.
        /// </summary>
        /// <param name="providerInvariantName">Name of the data provider (<see cref="System.Data.Common.DbProviderFactory"/>) to use.</param>
        /// <param name="connectionString">The connection string to use.</param>
        /// <returns>An object implementing <see cref="IDataEnvironment"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="connectionString"/> or <paramref name="connectionString"/></exception>
        IDataEnvironment GetByConnectionSetting(string providerInvariantName, string connectionString);

        /// <summary>
        /// Gets an environment for the specified connection string name.
        /// </summary>
        /// <param name="name">The connection string name to use.</param>
        /// <returns>An object implementing <see cref="IDataEnvironment"/>.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="name"/></exception>
        /// <exception cref="ArgumentException">No connection string with the specified name was found.</exception>
        IDataEnvironment GetByConnectionStringName(string name);

        /// <summary>
        /// Gets the default environment. The default environment is obtained using the <see cref="DatabaseConfiguration.DefaultName"/> property
        /// of the database configuration.
        /// </summary>
        /// <returns>An object implementing <see cref="IDataEnvironment"/>.</returns>
        IDataEnvironment GetDefault();
    }
}