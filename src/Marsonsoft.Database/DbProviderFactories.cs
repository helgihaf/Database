﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Marsonsoft.Database
{
    internal class DbProviderFactories : IDbProviderFactoryFactory
    {
        private class ProviderFactory
        {
            public Configuration.Provider Provider;
            public DbProviderFactory DbProviderFactory;
        }

        private readonly Dictionary<string, ProviderFactory> providerFactories;

        public DbProviderFactories(IEnumerable<Configuration.Provider> providerDefinitions)
        {
            if (providerDefinitions == null)
            {
                return;
            }
            providerFactories = providerDefinitions.Select(p => new ProviderFactory { Provider = p }).ToDictionary(f => f.Provider.InvariantName);
        }

        public DbProviderFactory GetFactory(string providerInvariantName)
        {
            if (!providerFactories.TryGetValue(providerInvariantName, out ProviderFactory providerFactory))
            {
                return null;
            }

            if (providerFactory.DbProviderFactory == null)
            {
                var dbProviderType = Type.GetType(providerFactory.Provider.AssemblyQualifiedName);
                if (dbProviderType == null)
                {
                    throw new ArgumentException($"Provider invariant name {providerInvariantName} is associated with DbProviderFactory named {providerFactory.Provider.AssemblyQualifiedName} but that type cannot be found.");
                }

                DbProviderFactory instance;
                var field = dbProviderType.GetField("Instance");
                if (field != null)
                {
                    instance = field.GetValue(null) as DbProviderFactory;
                    if (instance == null)
                    {
                        throw new ArgumentException($"Cannot create instance of type {providerFactory.Provider.AssemblyQualifiedName}. Instance field is null.");
                    }
                }
                else
                {
                    try
                    {
                        instance = Activator.CreateInstance(dbProviderType) as DbProviderFactory;
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException($"Cannot create instance of type {providerFactory.Provider.AssemblyQualifiedName}.", ex);
                    }
                }
                Interlocked.CompareExchange<DbProviderFactory>(ref providerFactory.DbProviderFactory, instance, null);
            }

            return providerFactory.DbProviderFactory;
        }
    }
}
