﻿using System.Collections.Generic;

namespace Marsonsoft.Database.Configuration
{
    /// <summary>
    /// Represents the configuration required to obtain an <see cref="IDataEnvironmentProvider"/> and ultimately an <see cref="IDataEnvironment"/>.
    /// </summary>
    public class DatabaseConfiguration
    {
        /// <summary>
        /// Gets or sets the default connection string name. Required, cannot be null.
        /// </summary>
        public string DefaultName { get; set; }

        /// <summary>
        /// Gets or sets the default provider name if specified, null if none is specified.
        /// </summary>
        public string DefaultProviderName { get; set; }

        /// <summary>
        /// Gets or sets the connection information list.
        /// </summary>
        public List<Connection> Connections { get; set; }

        /// <summary>
        /// Gets or sets the provider list.
        /// </summary>
        public List<Provider> Providers { get; set; }
    }
}
