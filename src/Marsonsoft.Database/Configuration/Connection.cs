﻿namespace Marsonsoft.Database.Configuration
{
    /// <summary>
    /// Represents a single, named connection string.
    /// </summary>
    public class Connection
    {
        /// <summary>
        /// Gets or sets the name of the connection string.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the provider name. This is the same as <see cref="Provider.InvariantName"/>.
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        public string ConnectionString { get; set; }

    }
}
