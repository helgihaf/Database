﻿namespace Marsonsoft.Database.Configuration
{
    /// <summary>
    /// Contains information about a provider that implements <see cref="System.Data.Common.DbProviderFactory"/>.
    /// </summary>
    public class Provider
    {
        /// <summary>
        /// Name that can be used programmatically to refer to the data provider.
        /// </summary>
        public string InvariantName { get; set; }

        /// <summary>
        /// Fully qualified name of the factory class, which contains enough information to instantiate the object.
        /// </summary>
        public string AssemblyQualifiedName { get; set; }
    }
}
