﻿using System.Data.Common;

namespace Marsonsoft.Database.Oracle
{
    /// <summary>
    /// Contains Oracle-specific extension methods for the <see cref="DbException"/> class.
    /// </summary>
    public static class DbExceptionExtensions
    {
        /// <summary>
        /// Gets a platform-agnostic <see cref="DatabaseError"/> value from the DbException.
        /// </summary>
        /// <param name="exception">In instance of an Oracle-specific exception class inheriting from <see cref="DbException"/>.</param>
        /// <returns>A <see cref="DatabaseError"/> value.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static DatabaseError GetDatabaseError(this DbException exception)
        {
            DatabaseError result = DatabaseError.Unknown;
            if (exception.GetType().FullName.StartsWith("Oracle.", System.StringComparison.Ordinal))
            {
                result = OracleExceptionNumber.ToDatabaseError(((dynamic)exception).Number);
            }

            return result;
        }
    }
}
