﻿using System.Data;
using System.Data.Common;

namespace Marsonsoft.Database.Oracle
{
    /// <summary>
    /// Contains extension methods for the <see cref="IDbCommand"/> interface..
    /// </summary>
    public static class DbCommandExtensions
    {
        /// <summary>
        /// Creates and adds a parameter of type OracleDbType.RefCursor to the specified command.
        /// </summary>
        /// <param name="command">The command to add the parameter to.</param>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <returns>A <see cref="DbParameter"/> object that was initialized with the specified parameters and has already been added to the <paramref name="command"/>.</returns>
        public static DbParameter AddParameterAsRefCursor(this IDbCommand command, string parameterName)
        {
            IAdapter adapter = AdapterFactory.GetAdapter(command.GetType().FullName);
            var parameter = adapter.CreateParameterAsRefCursor(parameterName);
            command.Parameters.Add(parameter);
            return parameter;
        }
    }
}
