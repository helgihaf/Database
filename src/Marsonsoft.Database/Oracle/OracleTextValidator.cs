﻿namespace Marsonsoft.Database.Oracle
{
    /// <summary>
    /// This class checks the validity of strings as quoted Oracle identifiers.
    /// </summary>
    public class OracleTextValidator : ITextValidator
    {
        private static readonly char[] invalidChars = new[] { '"', '\0' };

        // https://docs.oracle.com/cd/E11882_01/server.112/e41084/sql_elements008.htm#SQLRF00223:
        // Quoted identifiers can contain any characters and punctuations marks as well as spaces.
        // However, neither quoted nor nonquoted identifiers can contain double quotation marks
        // or the null character (\0).

        /// <summary>
        /// Checks if the specified user name is a valid Oracle user name identifier.
        /// </summary>
        /// <param name="userName">The user name to check.</param>
        /// <returns><c>true</c> if userName is a valid Oracle user name when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidUserName(string userName)
        {
            return IsValidIdentifier(userName);
        }

        /// <summary>
        /// Checks if the specified password is a valid Oracle password identifier.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <returns><c>true</c> if password is a valid Oracle password when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidPassword(string password)
        {
            return IsValidIdentifier(password);
        }

        /// <summary>
        /// Checks if the specified user name is a valid Oracle identifier.
        /// </summary>
        /// <param name="identifier">The identifier to check.</param>
        /// <returns><c>true</c> if identifier is a valid Oracle identifier when used quoted, <c>false</c> otherwise.</returns>
        public bool IsValidIdentifier(string identifier)
        {
            return
                !string.IsNullOrEmpty(identifier)
                &&
                identifier.Length <= 30
                &&
                identifier.IndexOfAny(invalidChars) == -1;
        }
    }
}
