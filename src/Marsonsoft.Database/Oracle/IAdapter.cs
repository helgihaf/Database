﻿using System.Data.Common;

namespace Marsonsoft.Database.Oracle
{
    internal interface IAdapter
    {
        DbParameter CreateParameterAsRefCursor(string parameterName);

        bool IsMyType(string typeName);
    }
}
