﻿using System;
using System.Data;
using System.Data.Common;

namespace Marsonsoft.Database.Oracle
{
    internal class NativeAdapter : IAdapter
    {
        private const string AssemblyName = "Oracle.DataAccess";
        private const string Namespace = "Oracle.DataAccess.Client";
        private const int RefCursorIntValue = 121; // Oracle.DataAccess.Client.OracleDbType.RefCursor = 121,

        private static readonly NativeAdapter instance = new NativeAdapter();

        private readonly LazyType oracleParameterType = new LazyType(Namespace + "OracleParameter", AssemblyName);
        private readonly LazyType oracleDbType = new LazyType(Namespace + ".OracleDbType", AssemblyName);

        private NativeAdapter()
        {
        }

        public static NativeAdapter Instance => instance;

        public DbParameter CreateParameterAsRefCursor(string parameterName)
        {
            // This is the statement we would use if we were referencing Oracle.DataAccess:
            //  return new OracleParameter(parameterName, Oracle.DataAccess.Client.OracleDbType.RefCursor, ParameterDirection.Output);
            // But this is what we do when we don't:
            return (DbParameter)Activator.CreateInstance(oracleParameterType.Value, Enum.ToObject(oracleDbType.Value, RefCursorIntValue), ParameterDirection.Output);
        }

        public bool IsMyType(string typeName)
        {
            return typeName.StartsWith(Namespace, StringComparison.Ordinal);
        }
    }
}
