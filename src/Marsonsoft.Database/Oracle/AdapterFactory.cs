﻿using System;

namespace Marsonsoft.Database.Oracle
{
    internal static class AdapterFactory
    {
        public static IAdapter GetAdapter(string typeName)
        {
            if (ManagedAdapter.Instance.IsMyType(typeName))
            {
                return ManagedAdapter.Instance;
            }
            else if (NativeAdapter.Instance.IsMyType(typeName))
            {
                return NativeAdapter.Instance;
            }

            throw new ArgumentException("Unknown type name");
        }
    }
}
