﻿using System;
using System.Data;

namespace Marsonsoft.Database.Oracle
{
    /// <summary>
    /// Contains Oracle-specific extension methods for the <see cref="IDbConnection"/> class.
    /// </summary>
    public static class DbConnectionExtensions
    {
        /// <summary>
        /// Creates an object implementing <see cref="IDbCommand"/> using the specified arguments.
        /// </summary>
        /// <param name="connection">The connection to which the command belongs.</param>
        /// <param name="cmdText">The command text.</param>
        /// <param name="commandType">The command type.</param>
        /// <param name="bindByName"><c>true</c> to bind parameters of the command by their name, <c>false</c> otherwise.</param>
        /// <returns>A new instance of the <see cref="IDbCommand"/> class.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static IDbCommand CreateCommand(this IDbConnection connection, string cmdText, CommandType commandType, bool bindByName)
        {
            var command = connection.CreateCommand();
            command.CommandText = cmdText;
            command.CommandType = commandType;
            ((dynamic)command).BindByName = bindByName;
            return command;
        }

        /// <summary>
        /// Configures the command using the Oracle-specific ClientId, ModuleName and ActionName properties. See <a href="http://docs.oracle.com/cd/E14435_01/win.111/e10927/OracleConnectionClass.htm#i999705"/>.
        /// </summary>
        /// <param name="connection">The connection to which the command belongs.</param>
        /// <param name="clientId">The client ID.</param>
        /// <param name="moduleName">The module name.</param>
        /// <param name="actionName">The action name.</param>
        public static void Configure(this IDbConnection connection, string clientId, string moduleName, string actionName)
        {
            Type connectionType = connection?.GetType() ?? throw new ArgumentNullException(nameof(connection));

            if (connectionType.Name != "OracleConnection")
            {
                throw new ArgumentException("Invalid type", nameof(connection));
            }

            dynamic dynamicConnection = connection;
            dynamicConnection.ClientId = clientId;
            dynamicConnection.ModuleName = moduleName;
            dynamicConnection.ActionName = actionName;
        }
    }
}
