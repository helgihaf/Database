﻿using System;
using System.Data;
using System.Data.Common;

namespace Marsonsoft.Database.Oracle
{
    internal sealed class ManagedAdapter : IAdapter
    {
        public const string Namespace = "Oracle.ManagedDataAccess.Client";
        private const string AssemblyName = "Oracle.ManagedDataAccess";
        private const int RefCursorIntValue = 121; // Oracle.ManagedDataAccess.Client.OracleDbType.RefCursor = 121,

        private static readonly ManagedAdapter instance = new ManagedAdapter();

        private readonly LazyType oracleParameterType = new LazyType(Namespace + ".OracleParameter", AssemblyName);
        private readonly LazyType oracleDbType = new LazyType(Namespace + ".OracleDbType", AssemblyName);

        private ManagedAdapter()
        {
        }

        public static ManagedAdapter Instance => instance;

        public DbParameter CreateParameterAsRefCursor(string parameterName)
        {
            // This is the statement we would use if we were referencing Oracle.ManagedDataAccess:
            //  return new OracleParameter(parameterName, Oracle.ManagedDataAccess.Client.OracleDbType.RefCursor, ParameterDirection.Output);
            // But this is what we do when we don't:
            return (DbParameter)Activator.CreateInstance(oracleParameterType.Value, parameterName, Enum.ToObject(oracleDbType.Value, RefCursorIntValue), ParameterDirection.Output);
        }

        public bool IsMyType(string typeName)
        {
            return typeName.StartsWith(Namespace, StringComparison.Ordinal);
        }
    }
}
