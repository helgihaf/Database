﻿namespace Marsonsoft.Database.Oracle
{
    internal static class OracleExceptionNumber
    {
        public const int ErrorAtRecursiveLevel = 604;
        public const int InvalidUserNameOrPassword = 1017;
        public const int IncorrectTnsName = 12154;

        public static DatabaseError ToDatabaseError(int number)
        {
            switch (number)
            {
                case InvalidUserNameOrPassword:
                    return DatabaseError.LoginFailure;
                case IncorrectTnsName:
                    return DatabaseError.InvalidDatabase;
                case ErrorAtRecursiveLevel:
                    return DatabaseError.GenericSQLError;
            }

            return DatabaseError.Unknown;
        }
    }
}
