﻿// Copyright 2017 Landsbankinn hf.
// Portions of this code are Copyright (C) 2011-2011 Topten Software (contact@toptensoftware.com)

using System;
using System.Reflection;

namespace Marsonsoft.Database.Oracle
{
    internal class LazyType
    {
        private readonly string typeName;
        private readonly string assemblyName;
        private Type value;

        public LazyType(string typeName, string assemblyName)
        {
            this.typeName = typeName ?? throw new ArgumentNullException(nameof(typeName));
            this.assemblyName = assemblyName;
        }

        public Type Value
        {
            get
            {
                if (value == null)
                {
                    value = TypeFromAssembly(typeName, assemblyName);
                }

                return value;
            }
        }

        public static Type TypeFromAssembly(string typeName, string assemblyName)
        {
            // Try to get the type from an already loaded assembly
            Type type = Type.GetType(typeName);

            if (type != null)
            {
                return type;
            }

            if (assemblyName == null)
            {
                // No assembly was specified for the type, so just fail
                string message = "Could not load type " + typeName + ". Possible cause: no assembly name specified.";
                throw new TypeLoadException(message);
            }

            Assembly assembly = Assembly.Load(assemblyName);

            if (assembly == null)
            {
                throw new InvalidOperationException("Can't find assembly: " + assemblyName);
            }

            type = assembly.GetType(typeName);

            if (type == null)
            {
                return null;
            }

            return type;
        }
    }
}
