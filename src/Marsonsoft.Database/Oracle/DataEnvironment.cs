﻿using System;
using System.Data.Common;

namespace Marsonsoft.Database.Oracle
{
    /// <summary>
    /// Represents an environment for working with database objects.
    /// </summary>
    [ProviderSupport("Oracle.ManagedDataAccess.Client")]
    [ProviderSupport("Oracle.DataAccess.Client")]
    public class DataEnvironment : IDataEnvironment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataEnvironment"/> class using the specified arguments.
        /// </summary>
        /// <param name="dbProviderFactory">The provider factory of the new environment.</param>
        /// <param name="connectionProvider">The connection provider of the new environment.</param>
        /// <exception cref="ArgumentNullException">Any of the specified arguments are null.</exception>
        public DataEnvironment(DbProviderFactory dbProviderFactory, IConnectionProvider connectionProvider)
        {
            DbProviderFactory = dbProviderFactory ?? throw new ArgumentNullException(nameof(dbProviderFactory));
            ConnectionProvider = connectionProvider ?? throw new ArgumentNullException(nameof(connectionProvider));
            TextValidator = new OracleTextValidator();
            Sql = new SqlFactoryAnsi92();
        }

        /// <summary>
        /// Gets the connection provider.
        /// </summary>
        public IConnectionProvider ConnectionProvider { get; private set; }

        /// <summary>
        /// Gets the provider factory.
        /// </summary>
        public DbProviderFactory DbProviderFactory { get; private set; }

        /// <summary>
        /// Gets the text validator.
        /// </summary>
        public ITextValidator TextValidator { get; private set; }

        /// <summary>
        /// Gets the <see cref="ISqlFactory"/> instance of this data environment.
        /// </summary>
        public ISqlFactory Sql { get; private set; }
    }
}
