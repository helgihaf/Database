﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;

namespace Marsonsoft.Database
{
    /// <summary>
    /// Contains extension methods for the <see cref="IDataRecord"/> interface.
    /// </summary>
    public static class DataRecordExtensions
    {
        /// <summary>
        /// Gets the value of the specified column name.
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="dataRecord">The data record to get value from.</param>
        /// <param name="name">The name of the column.</param>
        /// <returns>The value of the column.</returns>
        public static T GetValue<T>(this IDataRecord dataRecord, string name)
        {
            return ConvertValue<T>(dataRecord[name]);
        }

        /// <summary>
        /// Gets the value of the specified column name or, if NULL, the default value of <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="dataRecord">The data record to get value from.</param>
        /// <param name="name">The name of the column.</param>
        /// <returns>The value of the column or the default value of <typeparamref name="T"/> if the column value is null or <see cref="DBNull.Value"/>.</returns>
        public static T GetValueOrDefault<T>(this IDataRecord dataRecord, string name)
        {
            object value = dataRecord[name];
            if (value != null && value != DBNull.Value)
            {
                return ConvertValue<T>(value);
            }
            else
            {
                return default(T);
            }
        }

        /// <summary>
        /// Gets the value of the specified column name or, if NULL, the value of <paramref name="defaultValue"/>.
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="dataRecord">The data record to get value from.</param>
        /// <param name="name">The name of the column.</param>
        /// <param name="defaultValue">The default value to use if the reader's value is null or <see cref="DBNull.Value"/>.</param>
        /// <returns>The value of the column or the default value of <paramref name="defaultValue"/> if the column value is null or <see cref="DBNull.Value"/>.</returns>
        public static T GetValueOrDefault<T>(this IDataRecord dataRecord, string name, T defaultValue)
        {
            object value = dataRecord[name];
            if (value != null && value != DBNull.Value)
            {
                return ConvertValue<T>(value);
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Gets the field/column names of the data record.
        /// </summary>
        /// <param name="dataRecord">The data record to get names from.</param>
        /// <returns>The column names in the data record.</returns>
        public static IEnumerable<string> Names(this IDataRecord dataRecord)
        {
            for (int i = 0; i < dataRecord.FieldCount; i++)
            {
                yield return dataRecord.GetName(i);
            }
        }

        /// <summary>
        /// Gets the values of the data record.
        /// </summary>
        /// <param name="dataRecord">The data record to get values from.</param>
        /// <returns>The values in the data record.</returns>
        public static IEnumerable<object> Values(this IDataRecord dataRecord)
        {
            for (int i = 0; i < dataRecord.FieldCount; i++)
            {
                yield return dataRecord[i];
            }
        }

        private static T ConvertValue<T>(object value)
        {
            if (value is T)
            {
                return (T)value;
            }

            return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
        }
    }
}
