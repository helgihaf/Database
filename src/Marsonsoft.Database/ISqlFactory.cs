﻿namespace Marsonsoft.Database
{
    /// <summary>
    /// Represents the ability to create SQL elements.
    /// </summary>
    public interface ISqlFactory
    {
        /// <summary>
        /// Gets or sets a value controlling if and how identifiers created by the class implementing this interface
        /// are quoted.
        /// </summary>
        IdentifierQuoting IdentifierQuoting { get; set; }

        /// <summary>
        /// Returns the specified name as an identifier based on the rules set in <see cref="IdentifierQuoting"/>.
        /// </summary>
        /// <remarks>
        /// Examples:
        /// <p>customer_id => "customer_id"   (Oracle)</p>
        /// <p>customer_id => `customer_id`   (MySQL)</p>
        /// <p>customer_id => [customer_id]   (SQL Server)</p>
        /// </remarks>
        /// <param name="name">The name to represent as an SQL identifier.</param>
        /// <returns>The <paramref name="name"/> as an SQL identifier.</returns>
        string Identifier(string name);

        /// <summary>
        /// Returns the specified tble name as an identifier based on the rules set in <see cref="IdentifierQuoting"/>.
        /// </summary>
        /// <remarks>
        /// Examples (when <see cref="IdentifierQuoting"/> is <see cref="IdentifierQuoting.Always"/>):
        /// <p>my_schema.my_table => "my_schema"."my_table"   (Oracle)</p>
        /// <p>my_schema.my_table => `my_schema`.`my_table`   (MySQL)</p>
        /// <p>my_schema.my_table => [my_schema].[my_table]   (SQL Server)</p>
        /// </remarks>
        /// <param name="tableName">The table name to represent as an SQL identifier.</param>
        /// <returns>The <paramref name="tableName"/> as an SQL identifier.</returns>
        string Identifier(TableName tableName);

        /// <summary>
        /// Returns a valid SQL literal representation of the specified value.
        /// </summary>
        /// <param name="value">The value to get a literal for.</param>
        /// <returns>A valid SQL literal.</returns>
        string Literal(object value);
    }
}
