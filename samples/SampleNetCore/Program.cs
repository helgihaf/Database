﻿using Marsonsoft.Database;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;
using System.IO;

namespace SampleNetCore
{
    class Program
    {
        static void Main(string[] args)
        {
            // Build up a configuration from our json file
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json", optional: true)
                .Build();

            // Get a DatabaseConfiguration from our configuration
            var databaseConfiguration = configuration.Get<Marsonsoft.Database.Configuration.DatabaseConfiguration>();

            // Create a DataEnvironmentProvider for the DatabaseConfiguration
            var dataEnvironmentProvider = DataEnvironmentProviderFactory.Create(databaseConfiguration);

            // Get the default IDataEnvironment for our configuration
            var dataEnvironment = dataEnvironmentProvider.GetDefault();

            // Start using the dataEnvironment
            using (var connection = dataEnvironment.ConnectionProvider.OpenConnection())
            {
                // Use the IDbConnection to create the IDbCommand
                using (var command = connection.CreateCommand("select id, name from customers where type = :type"))
                {
                    // Extension method to add a parameter
                    command.AddParameter("type", "good");
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Use extension methods to get typed + named values from the reader
                            Debug.WriteLine($"id: {reader.GetValue<long>("id")}, name: {reader.GetValue<string>("name")}");
                        }
                    }
                }
            }

        }
    }
}
