﻿using Marsonsoft.Database;
using System.Configuration;
using Marsonsoft.Database.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Data.Common;
using System.Data;
using System.Diagnostics;

namespace SampleNetFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get a DatabaseConfiguration from app.config and existing .NET Framework conventions
            var databaseConfiguration = GetDatabaseConfiguration();

            // Create a DataEnvironmentProvider for the DatabaseConfiguration
            var dataEnvironmentProvider = DataEnvironmentProviderFactory.Create(databaseConfiguration);

            // Get the default IDataEnvironment for our configuration
            var dataEnvironment = dataEnvironmentProvider.GetDefault();

            // Start using the dataEnvironment
            using (var connection = dataEnvironment.ConnectionProvider.OpenConnection())
            {
                // Use the IDbConnection to create the IDbCommand
                using (var command = connection.CreateCommand("select id, name from customers where order_type = :order_type"))
                {
                    // Extension method to add a parameter
                    command.AddParameter("order_type", "good");
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // Use extension methods to get typed + named values from the reader
                            Debug.WriteLine($"id: {reader.GetValue<long>("id")}, name: {reader.GetValue<string>("name")}");
                        }
                    }
                }
            }

        }

        private static Marsonsoft.Database.Configuration.DatabaseConfiguration GetDatabaseConfiguration()
        {
            return new Marsonsoft.Database.Configuration.DatabaseConfiguration
            {
                DefaultName = ConfigurationManager.AppSettings["DefaultConnection"] ?? "DefaultConnection",
                DefaultProviderName = ConfigurationManager.AppSettings["DefaultProvider"],
                Connections = GetConnections(),
                Providers = GetProviders()
            };
        }

        private static List<Connection> GetConnections()
        {
            return ConfigurationManager.ConnectionStrings.Cast<ConnectionStringSettings>().Select(s => new Connection
            {
                Name = s.Name,
                ProviderName = s.ProviderName,
                ConnectionString = s.ConnectionString
            }).ToList();
        }

        private static List<Provider> GetProviders()
        {
            return (
                from row in DbProviderFactories.GetFactoryClasses().Rows.Cast<DataRow>()
                select new Provider
                {
                    InvariantName = row[2] as string,
                    AssemblyQualifiedName = row[3] as string
                }).ToList();
        }
    }
}
